VITE FULL NODE DOCKER IMAGE by lambdalaunchpad.com

To build:

  ./build.sh

To run:

1) Set environment variable to vite name and rewards address:

  export VITE_NODE_NAME=foobar
  export REWARDS_ADDR=vite_blablabla

2) Set vite data dir to where you want to store the vite blockchain on the host machine:
  
  export VITE_DATA_DIR=/home/foo/vite_data

3) Run:
  ./run.sh


