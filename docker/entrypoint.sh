#!/bin/bash

if [[ -z "$VITE_NODE_NAME" ]]; then
    echo "Must set VITE_NODE_NAME to your vite rewards address" 1>&2
    exit 1
fi

if [[ -z "$REWARDS_ADDR" ]]; then
    echo "Must set REWARDS_ADDR to your vite rewards address" 1>&2
    exit 1
fi

cd gvite-v2.12.0-linux
sed -i "s/\[VITE_NODE_NAME\]/$VITE_NODE_NAME/g" node_config.json
sed -i "s/\[REWARDS_ADDR\]/$REWARDS_ADDR/g" node_config.json
./bootstrap
tail -f gvite.log

