#!/bin/bash

if [[ -z "$VITE_NODE_NAME" ]]; then
    echo "Must set VITE_NODE_NAME to the node name" 1>&2
    exit 1
fi

if [[ -z "$REWARDS_ADDR" ]]; then
    echo "Must set REWARDS_ADDR to your vite rewards address" 1>&2
    exit 1
fi

if [[ -z "$VITE_DATA_DIR" ]]; then
    echo "Must set VITE_DATA_DIR" 1>&2
    exit 1
fi

name='vite_node'
[[ $(docker ps -f "name=$name" --format '{{.Names}}') == $name ]] || docker run -p 8483:8483 -p 8484:8484 -p 48132:48132 -p 41420:41420 --name "$name" --env VITE_NODE_NAME=$VITE_NODE_NAME --env REWARDS_ADDR=$REWARDS_ADDR -d -v $VITE_DATA_DIR:/root/.gvite lambdalaunchpad/vite_fullnode

